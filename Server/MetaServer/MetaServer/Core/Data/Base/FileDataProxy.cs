﻿using System.Collections.Generic;
using System.Linq;
using Common.Data;

namespace MetaServer.Core.Data.Base
{
    public class FileDataProxy : IDataProxy
    {
        public string RawData => string.Empty;

        public IList<string> FieldsNames => _fields.Keys.ToList();

        private readonly Dictionary<string, object> _fields = new Dictionary<string, object>();

        public bool ReadField<T>(string name, out T value)
        {
            object objValue;
            if (_fields.TryGetValue(name, out objValue))
            {
                value = (T) objValue;
                return true;
            }

            value = default(T);
            return true;
        }

        public void WriteField<T>(string name, T value)
        {
            _fields[name] = value;
        }
    }
}