﻿using System.Collections.Generic;
using Common.Data;

namespace MetaServer.Core.Data.Base
{
    public class GetScores : SimpleServerDataOperation
    {
        public int ClientId { get; }

        public Dictionary<int, ScoreSpec> Scores { get; }

        public override string Collection => DataTables.SCORES;

        public override DataOperationType OperationType => DataOperationType.Find;

        public GetScores(int clientId)
        {
            ClientId = clientId;
            Scores = new Dictionary<int, ScoreSpec>();
        }

        protected override void OnProcess(IDataCommand command)
        {
            base.OnProcess(command);

            IDataProxy inData = CreateDataProxy;

            inData.WriteField("playerId", ClientId);

            command.InData.Add(inData);
        }

        protected override void OnReadData(IDataCommand command)
        {
            foreach (var dataProxy in command.OutData)
            {
                ScoreSpec score = new ScoreSpec();

                dataProxy.ReadField("id", out score.id);
                dataProxy.ReadField("wins", out score.wins);
                dataProxy.ReadField("loses", out score.loses);

                Scores[score.id] = score;
            }
        }
    }
}