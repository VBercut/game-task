﻿using Common.Data;
using MetaServer.Core.Data.Game;

namespace MetaServer.Core.Data.Base
{
    public class TryLogin : SimpleServerDataOperation
    {
        public string Guid { get; }
        public int Id { get; }

        public Account Account { get; private set; }

        public override string Collection => DataTables.ACCOUNTS;
        public override DataOperationType OperationType => DataOperationType.Find;

        public TryLogin(string guid)
        {
            Guid = guid;
        }

        public TryLogin(int id)
        {
            Id = id;
        }

        protected override void OnProcess(IDataCommand command)
        {
            base.OnProcess(command);

            IDataProxy inData = CreateDataProxy;

            if (!string.IsNullOrEmpty(Guid))
            {
                inData.WriteField("guid", Guid);
            }
            else if (Id > 0)
            {
                inData.WriteField("id", Id);
            }

            command.InData.Add(inData);

            int i = 100;
            i = i + 10;
        }

        protected override void OnReadData(IDataCommand command)
        {
            if (Result == DataOperationResult.Okay && command.OutData.Count == 0)
            {
                Result = DataOperationResult.Error;
                return;
            }

            IDataProxy data = command.OutData[0];

            Account = new Account();

            data.ReadField("id", out Account.id);
            data.ReadField("guid", out Account.guid);
            data.ReadField("name", out Account.name);
        }
    }
}