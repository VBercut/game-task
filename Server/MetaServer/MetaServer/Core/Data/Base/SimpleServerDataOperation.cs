﻿using System;
using Common.Data;

namespace MetaServer.Core.Data.Base
{
    public enum DataOperationType
    {
        Insert,
        Update,
        Remove,
        Find
    }

    public abstract class SimpleServerDataOperation : DataOperation
    {
        public override IDataProxy CreateDataProxy => new FileDataProxy();

        public abstract string Collection { get; }
        public abstract DataOperationType OperationType { get; }

        protected override void OnProcess(IDataCommand command)
        {
            FileDataCommand cmd = (FileDataCommand) command;

            cmd.Collection = Collection;
            cmd.OperationType = OperationType;
        }

        protected virtual void WriteData(IDataCommand command, object data)
        {
            IDataProxy inData = CreateDataProxy;

            Type type = data.GetType();
            var fields = type.GetFields();

            foreach (var fieldInfo in fields)
            {
                inData.WriteField(fieldInfo.Name, fieldInfo.GetValue(data));
            }

            command.InData.Add(inData);
        }
    }
}