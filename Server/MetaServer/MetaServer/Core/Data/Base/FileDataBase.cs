﻿using Common.Data;
using LiteDB;
using MetaServer.Core.Data.Game;

namespace MetaServer.Core.Data
{
    public class FileDataBase : IDataBase
    {
        private LiteDatabase _connect;

        public bool Init()
        {
            BsonMapper mapper = BsonMapper.Global;

            mapper.IncludeFields = true;

            mapper.Entity<Account>()
                .Id(x => x.id)
                .Field(x => x.guid, "guid")
                .Field(x => x.name, "name")
                .Index(x => x.guid, true)
                .Index(x => x.name, true);

            mapper.Entity<ScoreSpec>()
                .Id(x => x.id)
                .Field(x => x.playerId, "playerId")
                .Field(x => x.heroId, "heroId")
                .Field(x => x.wins, "wins")
                .Field(x => x.loses, "loses")
                .Index(x => x.playerId);

            _connect = new LiteDatabase(@"Game.db", BsonMapper.Global);

            return true;
        }

        public IDataCommand CreateCommand()
        {
            FileDataCommand command = new FileDataCommand(_connect);

            return command;
        }

        public IDataTransaction BeginTransaction()
        {
            FileDataTransaction transaction = new FileDataTransaction();

            return transaction;
        }

        public void Dispose()
        {
            _connect?.Dispose();
        }
    }
}