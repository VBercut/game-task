﻿using Common.Data;
using MetaServer.Core.Data.Game;

namespace MetaServer.Core.Data.Base
{
    public class SaveAccount : SimpleServerDataOperation
    {
        public Account Account { get; }

        public override string Collection => DataTables.ACCOUNTS;

        public SaveAccount(Account account)
        {
            Account = account;
        }

        protected override void OnProcess(IDataCommand command)
        {
            base.OnProcess(command);

            WriteData(command, Account);
        }

        public override DataOperationType OperationType => DataOperationType.Update;
    }
}