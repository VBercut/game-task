﻿using System.Collections.Generic;
using Common.Data;
using LiteDB;
using MetaServer.Core.Data.Base;

namespace MetaServer.Core.Data
{
    public class FileDataCommand : IDataCommand
    {
        public IDataTransaction Transaction { get; set; }

        public LiteDatabase Connect { get; }

        public IList<IDataProxy> InData
        {
            get { return _inData; }
        }

        public IList<IDataProxy> OutData
        {
            get { return _outData; }
        }

        public string Collection { get; set; }
        public DataOperationType OperationType { get; set; }

        public FileDataCommand(LiteDatabase connect)
        {
            Connect = connect;
        }

        private readonly List<IDataProxy> _inData = new List<IDataProxy>();
        private readonly List<IDataProxy> _outData = new List<IDataProxy>();

        public virtual DataOperationResult Execute()
        {
            switch (OperationType)
            {
                case DataOperationType.Insert:
                {
                    Insert();
                    break;
                }

                case DataOperationType.Update:
                {
                    Update();
                    break;
                }

                case DataOperationType.Find:
                {
                    Find();
                    break;
                }

                case DataOperationType.Remove:
                {
                    Remove();
                    break;
                }
            }

            return DataOperationResult.Okay;
        }

        public void Cancel()
        {
        }

        public object Clone()
        {
            return new FileDataCommand(Connect);
        }

        public void Dispose()
        {
        }

        /// <summary>
        /// TODO: MOST OF ALL THEM ARE TO SLOW. NEED TO REVIEW OPERATIONS.
        /// </summary>
        void Find()
        {
            if (!Connect.CollectionExists(Collection))
            {
                return;
            }

            var collection = Connect.GetCollection(Collection);

            IDataProxy dataProxy = InData[0];

            foreach (string fieldName in dataProxy.FieldsNames)
            {
                object value;
                if (!dataProxy.ReadField(fieldName, out value))
                {
                    continue;
                }

                var storedFieldName = fieldName;
                collection = collection.Include(x => x.RawValue[storedFieldName].RawValue.Equals(value));
            }

            var result = collection.FindAll();

            foreach (BsonDocument entry in result)
            {
                IDataProxy data = new FileDataProxy();

                foreach (var bsonValue in entry.RawValue)
                {
                    data.WriteField(bsonValue.Key, bsonValue.Value.RawValue);
                }

                OutData.Add(data);
            }
        }

        private void Remove()
        {
            if (!Connect.CollectionExists(Collection))
            {
                return;
            }

            using (var transcation = Connect.BeginTrans())
            {
                var collection = Connect.GetCollection(Collection);

                foreach (var dataProxy in InData)
                {
                    var dataToDelete = collection;

                    foreach (string fieldName in dataProxy.FieldsNames)
                    {
                        object value;
                        if (!dataProxy.ReadField(fieldName, out value))
                        {
                            continue;
                        }

                        dataToDelete = dataToDelete.Include(x => x.RawValue[fieldName].RawValue.Equals(value));
                    }

                    var result = dataToDelete.FindAll();

                    foreach (BsonDocument data in result)
                    {
                        collection.Delete(data.RawValue["id"]);
                    }
                }

                transcation.Commit();
            }
        }

        private void Update()
        {
            if (!Connect.CollectionExists(Collection))
            {
                return;
            }

            using (var transcation = Connect.BeginTrans())
            {
                var collection = Connect.GetCollection(Collection);

                foreach (var dataProxy in InData)
                {
                    object id;
                    if (!dataProxy.ReadField("id", out id))
                    {
                        continue;
                    }

                    var entry = collection.FindOne(Query.EQ("id", new BsonValue(id)));

                    foreach (string fieldName in dataProxy.FieldsNames)
                    {
                        if (fieldName == "id")
                        {
                            continue;
                        }

                        object value;
                        if (!dataProxy.ReadField(fieldName, out value))
                        {
                            continue;
                        }

                        entry.RawValue[fieldName] = new BsonValue(value);
                    }

                    collection.Update(entry);
                }
            }
        }

        private void Insert()
        {
            using (var transcation = Connect.BeginTrans())
            {
                var collection = Connect.GetCollection(Collection);

                foreach (var dataProxy in InData)
                {
                    Dictionary<string, BsonValue> values = new Dictionary<string, BsonValue>();

                    foreach (string fieldName in dataProxy.FieldsNames)
                    {
                        object value;
                        if (!dataProxy.ReadField(fieldName, out value))
                        {
                            continue;
                        }

                        values[fieldName] = new BsonValue(value);
                    }

                    collection.Insert(new BsonDocument(values));
                }

                transcation.Commit();
            }
        }
    }
}