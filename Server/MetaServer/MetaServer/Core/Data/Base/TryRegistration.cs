﻿using Common.Data;

namespace MetaServer.Core.Data.Base
{
    public class TryRegistration : SimpleServerDataOperation
    {
        public string Guid { get; }

        public override string Collection => DataTables.ACCOUNTS;

        public TryRegistration(string guid)
        {
            Guid = guid;
        }

        protected override void OnProcess(IDataCommand command)
        {
            base.OnProcess(command);

            IDataProxy inData = CreateDataProxy;

            inData.WriteField("guid", Guid);

            command.InData.Add(inData);
        }

        protected override void OnReadData(IDataCommand command)
        {
        }

        public override DataOperationType OperationType => DataOperationType.Insert;
    }
}