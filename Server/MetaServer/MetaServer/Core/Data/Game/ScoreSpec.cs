﻿namespace MetaServer.Core.Data
{
    public class ScoreSpec
    {
        public int id;
        public int playerId;

        public int heroId;
        public int wins;
        public int loses;
    }
}