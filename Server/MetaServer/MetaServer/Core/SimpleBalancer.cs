﻿using Common.Core;

namespace MetaServer.Core
{
    public class SimpleBalancer : IBalancer
    {
        public void AddServer(IServerPeer server)
        {
        }

        public void RemoveServer(IServerPeer server)
        {
        }

        public void AddPlayer(IClient client)
        {
            
        }

        public void RemoveClient(IClient client)
        {
        }
    }
}
