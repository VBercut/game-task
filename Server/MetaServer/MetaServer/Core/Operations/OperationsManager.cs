﻿using System;
using System.Collections.Generic;
using Common.Core;
using Common.Data;

namespace MetaServer.Core.Operations
{
    public class OperationsManager : IOperationManager
    {
        private readonly Dictionary<OperationType, IOperation> _operations =
            new Dictionary<OperationType, IOperation>();

        public void AddOperation(IOperation operation)
        {
            if (!_operations.ContainsKey(operation.Type))
            {
                operation.Manager = this;
                _operations[operation.Type] = operation;
            }
        }

        public void RemoveOperation(OperationType type)
        {
            IOperation operation;

            if (_operations.TryGetValue(type, out operation))
            {
                operation.Manager = null;
                _operations.Remove(type);
            }
        }

        public Dictionary<byte, object> ProcessRequest(OperationType type, Dictionary<byte, object> data,
            out ReturnCode result)
        {
            Dictionary<byte, object> responce = null;

            result = ReturnCode.OperationIsNotSupported;

            if (!CheckOperation(type, data, out responce))
            {
                return responce;
            }

            IOperation operation = _operations[type];
            using (IOperationHandler handler = operation.CreateHandler())
            {
                OperationResultCode operationResultCode = handler.Start(data);

                if (operationResultCode == OperationResultCode.InProgress)
                {
                    while (operationResultCode == OperationResultCode.InProgress)
                    {
                        operationResultCode = handler.Update();
                    }

                    operationResultCode = handler.Finish();
                }

                result = ReturnCode.InvalidOperation;

                if (operationResultCode == OperationResultCode.Failed)
                {
                    return CreateInvalidOperation(data);
                }

                result = ReturnCode.Ok;

                IOperationData operationData = handler.Responce;
                if (operationData == null)
                {
                    return null;
                }

                responce = operationData.Serialize();
                responce[(byte) OperationFields.OperationId] = data[(byte) OperationFields.OperationId];

                return responce;
            }
        }

        private bool CheckOperation(OperationType type, Dictionary<byte, object> data,
            out Dictionary<byte, object> responce)
        {
            responce = null;

            if (_operations.ContainsKey(type))
            {
                return true;
            }

            responce = CreateInvalidOperation(data);

            return false;
        }

        private Dictionary<byte, object> CreateInvalidOperation(Dictionary<byte, object> data)
        {
            return new Dictionary<byte, object>
            {
                [(byte) OperationFields.OperationId] = data[(byte) OperationFields.OperationId]
            };
        }

        public bool ProcessResponce(OperationType type, Dictionary<byte, object> data, ReturnCode result)
        {
            throw new NotSupportedException();
        }

        public void Update()
        {
            throw new NotSupportedException();
        }
    }
}