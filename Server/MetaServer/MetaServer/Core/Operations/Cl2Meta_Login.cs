﻿using System;
using Common.Core;
using Common.Data;
using MetaServer.Core.Data.Base;

namespace MetaServer.Core.Operations
{
    public class Cl2Meta_Login : Common.Core.Operations.Cl2Meta_Login
    {
        private Client Client { get; set; }

        public Cl2Meta_Login(Client client)
        {
            Client = client;
        }

        public override IOperationHandler CreateHandler()
        {
            return new Handler(Client);
        }

        public class Handler : MetaServerOperationHandler<Request, Responce>
        {
            internal Handler(Client client) : base(client)
            {
            }

            public override OperationResultCode OnStart()
            {
                TryLogin tryLogin = this.DoLogin();

                if (tryLogin.Result == DataOperationResult.Error)
                {
                    return OperationResultCode.Failed;
                }

                if (Application.Server.Reconnect(Client))
                {
                    tryLogin.Process(Application.Server.DataBase);
                }

                Client.Token = Guid.NewGuid().ToString();
                Client.Account = tryLogin.Account;

                if (!Client.Login())
                {
                    return OperationResultCode.Failed;
                }

                CollectScore(tryLogin);

                Application.Server.AddClient(Client);

                return OperationResultCode.Success;
            }

            private void CollectScore(TryLogin tryLogin)
            {
                GetScores getScores = new GetScores(tryLogin.Id);
                getScores.Process(Application.Server.DataBase);

                Client.Score = getScores.Scores;
            }

            private TryLogin DoLogin()
            {
                TryLogin tryLogin = new TryLogin(RequestData.guid);
                tryLogin.Process(Application.Server.DataBase);

                if (tryLogin.Result == DataOperationResult.Error)
                {
                    TryRegistration tryRegistration = new TryRegistration(RequestData.guid);
                    tryRegistration.Process(Application.Server.DataBase);

                    if (tryRegistration.Result == DataOperationResult.Error)
                    {
                        return null;
                    }

                    // Repeat After Registration
                    tryLogin.Process(Application.Server.DataBase);
                }

                return tryLogin;
            }
        }
    }
}