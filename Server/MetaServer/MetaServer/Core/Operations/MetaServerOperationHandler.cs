﻿using System;
using System.Collections.Generic;
using Common.Core;
using Common.Core.Operations;

namespace MetaServer.Core.Operations
{
    public class MetaServerOperationHandler<TRequest, TResponce> : BaseOperationHandler<TRequest, TResponce>
        where TRequest : IOperationData
        where TResponce : IOperationData
    {
        protected Client Client { get; private set; }

        public MetaServerOperationHandler(Client client)
        {
            Client = client;
            Responce = Activator.CreateInstance<TResponce>();
        }

        protected override void ReadData(Dictionary<byte, object> data)
        {
            Request = Activator.CreateInstance<TRequest>();
            Request.Deserialize(data);
        }
    }
}