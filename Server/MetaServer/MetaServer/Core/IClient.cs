﻿using System;

namespace MetaServer.Core
{
    public enum ClientState : byte
    {
        Invalid,
        Valid,
        WaitingGame,
        PlayingGame
    }

    public interface IClient
    {
        event Action<IClient> OnDisconnected; 

        int Id { get; }

        ClientState State { get; set; }

        bool IsRemoved { get; set; }

        void Disconnect();
        void Save();
    }
}