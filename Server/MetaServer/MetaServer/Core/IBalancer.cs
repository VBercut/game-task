﻿using Common.Core;

namespace MetaServer.Core
{
    public interface IBalancer
    {
        void AddServer(IServerPeer server);
        void RemoveServer(IServerPeer server);

        void AddPlayer(IClient client);
        void RemoveClient(IClient client);
    }
}