﻿using Common.Core;
using Common.Data;

namespace MetaServer.Core
{
    public interface IMetaServer
    {
        IDataBase DataBase { get; }

        void OnGameServerDisconnect(IServerPeer server);

        IClient GetClient(int clientId);

        void AddClient(IClient client);
        void RemoveClient(IClient client);
        bool Reconnect(IClient client);
    }
}