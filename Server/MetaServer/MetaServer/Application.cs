﻿using System.Collections.Generic;
using Common;
using Common.Core;
using Common.Data;
using MetaServer.Core;
using MetaServer.Core.Data;
using Photon.SocketServer;

namespace MetaServer
{
    public class Application : ApplicationBase, IMetaServer
    {
        public static IMetaServer Server { get; private set; }

        #region Fields

        public IDataBase DataBase { get; private set; }

        private List<IServerPeer> _gameServers = new List<IServerPeer>();
        private Dictionary<int, IClient> _clients = new Dictionary<int, IClient>();

        private IBalancer _balancer;

        #endregion

        #region Inbounds

        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            if (initRequest.LocalPort == StaticData.Instance.GameServer.Port)
            {
                GameServerClient gameServer = new GameServerClient(initRequest);

                lock (_gameServers)
                {
                    _gameServers.Add(gameServer);
                }
                _balancer.AddServer(gameServer);

                return gameServer;
            }

            return new Client(initRequest);
        }

        #endregion

        #region Init

        static Application()
        {
            // System.Windows.Forms.MessageBox.Show("Meta Server inited!");
        }

        protected override void Setup()
        {
            Server = this;

            Initialize();
        }

        private void Initialize()
        {
            Protocol.AllowRawCustomValues = true;

            _balancer = new SimpleBalancer();

            DataBase = new FileDataBase();

            DataBase.Init();

            InitSerializationHandlers();
        }

        private void InitSerializationHandlers()
        {
        }

        protected override void TearDown()
        {
        }

        #endregion

        #region Public API

        public IClient GetClient(int clientId)
        {
            lock (_clients)
            {
                IClient client;
                if (_clients.TryGetValue(clientId, out client))
                {
                    return client;
                }
            }

            return null;
        }

        public void AddClient(IClient client)
        {
            lock (_clients)
            {
                _clients.Add(client.Id, client);
            }
        }

        public void RemoveClient(IClient client)
        {
            lock (_clients)
            {
                if (client.IsRemoved)
                {
                    return;
                }

                _balancer.RemoveClient(client);
                _clients.Remove(client.Id);

                client.IsRemoved = true;
            }
        }

        public void OnGameServerDisconnect(IServerPeer server)
        {
            lock (_gameServers)
            {
                _gameServers.Remove(server);
            }

            _balancer.RemoveServer(server);
        }

        public bool Reconnect(IClient client)
        {
            IClient existClient = GetClient(client.Id);

            if (existClient == null)
            {
                return false;
            }

            RemoveClient(client);
            client.Save();
            client.Disconnect();

            return true;
        }

        #endregion
    }
}