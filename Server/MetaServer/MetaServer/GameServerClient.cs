﻿using Common;
using Common.Core;
using Photon.SocketServer;
using Photon.SocketServer.ServerToServer;
using PhotonHostRuntimeInterfaces;

namespace MetaServer
{
    public class GameServerClient : InboundS2SPeer, IServerPeer
    {
        public string ConnectionString => StaticData.Instance.GameServer.Ip + ":" + StaticData.Instance.GameServer.Port;

        public GameServerClient(InitRequest initRequest) : base(initRequest)
        {
        }

        public GameServerClient(InitResponse response) : base(response)
        {
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail)
        {
        }

        protected override void OnEvent(IEventData eventData, SendParameters sendParameters)
        {
        }

        protected override void OnOperationResponse(OperationResponse operationResponse, SendParameters sendParameters)
        {
        }
    }
}