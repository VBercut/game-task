﻿using System;
using System.Collections.Generic;
using Common.Core;
using Common.Data;
using MetaServer.Core;
using MetaServer.Core.Data;
using MetaServer.Core.Data.Base;
using MetaServer.Core.Data.Game;
using MetaServer.Core.Operations;
using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using IOperationHandler = Photon.SocketServer.Rpc.IOperationHandler;

namespace MetaServer
{
    public class Client : Peer, IClient, IOperationHandler
    {
        IOperationManager _operationManager = new OperationsManager();

        public event Action<IClient> OnDisconnected;

        public int Id { get; }
        public ClientState State { get; set; }

        public bool IsRemoved { get; set; }

        public string Token { get; set; }
        public Account Account { get; set; }

        public Dictionary<int, ScoreSpec> Score { get; set; }


        public Client(InitRequest initRequest) : base(initRequest)
        {
            IsRemoved = false;
            SetCurrentOperationHandler(this);

            State = ClientState.Invalid;

            _operationManager.AddOperation(new Cl2Meta_Login(this));
        }

        public OperationResponse OnOperationRequest(PeerBase peer, OperationRequest operationRequest,
            SendParameters sendParameters)
        {
            ReturnCode returnCode;

            Dictionary<byte, object> responce =
                _operationManager.ProcessRequest((OperationType) operationRequest.OperationCode,
                    operationRequest.Parameters, out returnCode);

            // Manual sending
            if (responce == null && returnCode == ReturnCode.Ok)
            {
                return null;
            }

            OperationResponse operationResponse =
                new OperationResponse(operationRequest.OperationCode, responce)
                {
                    ReturnCode = (short) returnCode
                };

            return operationResponse;
        }

        public void Save()
        {
            if (Account == null)
            {
                return;
            }

            SaveAccount saveAccount = new SaveAccount(Account);
            saveAccount.Process(Application.Server.DataBase);
        }

        public void OnDisconnect(PeerBase peer)
        {
            OnDisconnected?.Invoke(this);

            Application.Server.RemoveClient(this);
            Save();
            SetCurrentOperationHandler(null);
            Dispose();
        }

        public bool Login()
        {
            if (State == ClientState.Invalid)
            {
                State = ClientState.Valid;
                return true;
            }

            return false;
        }
    }
}