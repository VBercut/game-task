﻿using System;
using System.Collections.Generic;
using Client.MetaServer.Data;
using Client.MetaServer.Operations;
using Common.Core;
using Common.Core.Events;
using Common.Data;
using ExitGames.Client.Photon;
using UnityEngine;
using EventData = ExitGames.Client.Photon.EventData;

namespace Client.MetaServer
{
    public enum MetaServerConnectStates
    {
        Disconnect,
        WaitingForConnect,
        Connect
    }

    public class MetaServer : IPhotonPeerListener, IEventCreator
    {
        #region Fields

        public static MetaServer Instance { get; private set; }

        public event Action OnChanged;

        public MetaServerConnectStates State { get; private set; }

        public Client Client
        {
            get { return _client; }
        }

        public IMetaServerListener Listener
        {
            get { return _listener; }
        }

        public ServerSettings Settings
        {
            get { return _settings; }
        }

        #region Data From Server

        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }

        public Dictionary<int, Score> Scores { get; private set; }

        #endregion

        private Client _client;

        private readonly IMetaServerListener _listener;
        private readonly ServerSettings _settings;

        #endregion

        #region Init

        public MetaServer(IMetaServerListener listener, ServerSettings serverSettings)
        {
            _listener = listener;
            _settings = serverSettings;
            State = MetaServerConnectStates.Disconnect;

            Instance = this;
        }

        public bool Init(Client client)
        {
            _client = client;
            _client.DisconnectTimeout = ServerSettings.DisconnectTimeout;

            if (!_client.Connect(_settings.ResultServerAddress, "MetaServer"))
            {
                return false;
            }

            State = MetaServerConnectStates.WaitingForConnect;

            _client.OperationManager.AddOperation(new Cl2Meta_Login());

            return true;
        }

        #endregion

        #region Operations

        public bool Request(OperationType type, IOperationHandler handler, bool reliable = false)
        {
            return Request(type, handler, null, reliable);
        }

        public bool Request(OperationType type, IOperationHandler handler,
            Action<IOperationHandler, OperationResultCode> callback, bool reliable = false)
        {
            if (_client == null)
            {
                return false;
            }

            return _client.OperationManager.Request(type, handler, callback, reliable);
        }

        #endregion

        #region Photon API

        public void OnOperationResponse(OperationResponse operationResponse)
        {
            _client.OperationManager.ProcessResponce((OperationType) operationResponse.OperationCode,
                operationResponse.Parameters,
                (ReturnCode) operationResponse.ReturnCode);
        }

        public void OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                {
                    State = MetaServerConnectStates.Connect;
                    _listener.Connect();

                    break;
                }

                case StatusCode.Disconnect:
                case StatusCode.DisconnectByServer:
                case StatusCode.DisconnectByServerLogic:
                case StatusCode.DisconnectByServerUserLimit:
                case StatusCode.TimeoutDisconnect:
                {
                    State = MetaServerConnectStates.Disconnect;
                    _listener.OnDisconnect(statusCode);

                    break;
                }
            }
        }

        public void OnEvent(EventData eventData)
        {
            throw new NotImplementedException();
        }

        public void DebugReturn(DebugLevel level, string message)
        {
            Debug.Log(string.Format("{0} : {1}", level, message));
        }

        public IEventHandler CreateEvent(EventDataType type)
        {
            switch (type)
            {
                case EventDataType.Meta2Cl_UpdateScore:
                    return new Common.Core.Events.EventHandler<Meta2Cl_UpdateScore>(OnUpdateScores);
            }

            return null;
        }

        #endregion

        #region Public API

        public void Update()
        {
            if (Client != null)
            {
                Client.Service();
            }
        }

        #endregion

        #region Process Events

        private void OnUpdateScores(Meta2Cl_UpdateScore eventData)
        {
            Scores.Clear();

            for (int i = 0; i < eventData.id.Length; i++)
            {
                Score score = new Score
                {
                    id = eventData.id[i],
                    wins = eventData.wins[i],
                    loses = eventData.loses[i]
                };

                Scores[score.id] = score;
            }

            if (OnChanged != null)
            {
                OnChanged();
            }
        }

        #endregion
    }
}