﻿using ExitGames.Client.Photon;

namespace Client.MetaServer
{
    public interface IMetaServerListener
    {
        void Connect();

        void Login(int accountId, string token);

        void OnDisconnect(StatusCode statusCode);
    }
}