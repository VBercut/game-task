﻿using System;
using Common.Core;
using Common.Core.Operations;

namespace Client.MetaServer.Operations
{
    public class Cl2Meta_Login : Common.Core.Operations.Cl2Meta_Login
    {
        public static Handler SendRequest(MetaServer server, string guid, Action<IOperationHandler, OperationResultCode> callback = null)
        {
            Handler handler = new Handler(server);
            handler.RequestData.guid = guid;

            server.Request(OperationType.Cl2Meta_Login, handler, callback, true);

            return handler;
        }

        public class Handler : BaseOperationHandler<Request, Responce>
        {
   
            public Handler(MetaServer metaServer) : base(metaServer, new Request(), new Responce())
            {
            }

            public override OperationResultCode OnStart()
            {
                if (ResponceData.result != LoginResult.Success)
                {
                    return OperationResultCode.Failed;
                }

                _metaServer.AccountId = ResponceData.id;
                _metaServer.Name = ResponceData.name;

                _metaServer.Token = ResponceData.token;

                _metaServer.Listener.Login(ResponceData.id, ResponceData.token);

                return OperationResultCode.Success;
            }
        }
    }
}