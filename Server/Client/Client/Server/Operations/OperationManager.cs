﻿using System;
using System.Collections.Generic;
using Common.Core;
using Common.Data;

namespace Client.MetaServer.Operations
{
    public class ProcessOperationData
    {
        public IOperationHandler Handler;
        public Action<IOperationHandler, OperationResultCode> Callback;

        public void OnCallback(OperationResultCode result)
        {
            var handler = Callback;

            if (handler != null)
            {
                handler(Handler, result);
            }
        }
    }

    public class OperationManager : IOperationManager
    {
        private Client _client;
        private int _operationId;

        private Dictionary<OperationType, IOperation> _operations = new Dictionary<OperationType, IOperation>();
        private Dictionary<int, ProcessOperationData> _processOperations = new Dictionary<int, ProcessOperationData>();

        private int NextOperationId
        {
            get { return ++_operationId; }
        }

        public OperationManager(Client client)
        {
            _client = client;
        }

        public void AddOperation(IOperation operation)
        {
            if (_operations.ContainsKey(operation.Type))
            {
                return;
            }

            operation.Manager = this;
            _operations[operation.Type] = operation;
        }

        public void RemoveOperation(OperationType type)
        {
            IOperation operation;
            if (_operations.TryGetValue(type, out operation))
            {
                operation.Manager = null;
                _operations.Remove(type);
            }
        }

        public void Update()
        {
            foreach (var operation in _processOperations.Values)
            {
                operation.Handler.WaitResponce();
            }
        }

        public bool Request(OperationType type,
            IOperationHandler handler,
            Action<IOperationHandler, OperationResultCode> callback,
            bool reliable = false
        )
        {
            if (!_operations.ContainsKey(type))
            {
                return false;
            }

            int operationId = NextOperationId;

            _processOperations.Add(operationId, new ProcessOperationData
            {
                Handler = handler,
                Callback = callback
            });

            var data = handler.Request.Serialize();
            data[(byte) OperationFields.OperationId] = operationId;

            return _client.OpCustom((byte) type, data, reliable);
        }

        public Dictionary<byte, object> ProcessRequest(OperationType type, Dictionary<byte, object> data,
            out ReturnCode result)
        {
            throw new NotSupportedException();
        }

        public bool ProcessResponce(OperationType type, Dictionary<byte, object> data, ReturnCode result)
        {
            int operationId = -1;

            if (data.ContainsKey((byte) OperationFields.OperationId))
            {
                operationId = (int) data[(byte) OperationFields.OperationId];
            }

            ProcessOperationData activeOperation = null;
            IOperationHandler handler = null;

            if (operationId > 0)
            {
                if (!_processOperations.ContainsKey(operationId))
                {
                    return false;
                }

                activeOperation = _processOperations[operationId];
                handler = activeOperation.Handler;

                _processOperations.Remove(operationId);
            }
            else
            {
                if (!_operations.ContainsKey(type))
                {
                    return false;
                }

                handler = _operations[type].CreateHandler();
            }

            using (handler)
            {
                if (result == ReturnCode.Ok)
                {
                    OperationResultCode operationResult = handler.Start(data);

                    while (operationResult == OperationResultCode.InProgress)
                    {
                        operationResult = handler.Update();
                    }

                    handler.Finish();

                    if (activeOperation != null)
                    {
                        activeOperation.OnCallback(operationResult);
                    }
                }
                else
                {
                    handler.ProcessFail(result);

                    if (activeOperation != null)
                    {
                        activeOperation.OnCallback(OperationResultCode.Failed);
                    }
                }
            }

            return true;
        }
    }
}