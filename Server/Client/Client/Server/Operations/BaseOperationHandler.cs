﻿using System.Collections.Generic;
using Common.Core;
using Common.Data;

namespace Client.MetaServer.Operations
{
    /// <summary>
    /// Support AOT Compilation
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    /// <typeparam name="TResponce"></typeparam>
    public abstract class BaseOperationHandler<TRequest, TResponce> : IOperationHandler
        where TRequest : IOperationData where TResponce : IOperationData
    {
        public IOperationData Request { get; private set; }
        public IOperationData Responce { get; private set; }

        public TRequest RequestData
        {
            get { return (TRequest) Request; }
        }

        public TResponce ResponceData
        {
            get { return (TResponce) Responce; }
        }

        protected MetaServer _metaServer { get; private set; }

        protected BaseOperationHandler(MetaServer metaServer, TRequest request, TResponce responce)
        {
            Request = request;
            Responce = responce;

            _metaServer = metaServer;
        }

        public OperationResultCode Start(Dictionary<byte, object> data)
        {
            ReadData(data);

            return OnStart();
        }

        protected virtual void ReadData(Dictionary<byte, object> data)
        {
            Responce.Deserialize(data);
        }

        public virtual OperationResultCode OnStart()
        {
            return OperationResultCode.Success;
        }

        public OperationResultCode Update()
        {
            return OnUpdate();
        }

        public virtual OperationResultCode OnUpdate()
        {
            return OperationResultCode.Success;
        }

        public OperationResultCode Finish()
        {
            return OnFinish();
        }

        public virtual OperationResultCode OnFinish()
        {
            return OperationResultCode.Success;
        }

        public virtual void WaitResponce()
        {
        }

        public virtual void ProcessFail(ReturnCode code)
        {
        }

        public virtual void Dispose()
        {
        }
    }
}