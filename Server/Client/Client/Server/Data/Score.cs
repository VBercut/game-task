﻿namespace Client.MetaServer.Data
{
    public class Score
    {
        public int id;

        public int wins;
        public int loses;
    }
}
