﻿using System;
using System.Net;
using System.Net.Sockets;
using Client.MetaServer.Utils;
using UnityEngine;

namespace Client.MetaServer
{
    [Serializable]
    public class ServerSettings
    {
        public static byte ChannelsCount { get; private set; }
        public static int DisconnectTimeout { get; private set; }

        static ServerSettings()
        {
            ChannelsCount = 3;
            DisconnectTimeout = 50000;
        }

        public string ServerName { get; set; }
        public string ServerAddress { get; set; }

        // Issues with IPv6
        public string ResultServerAddress
        {
            get
            {
                return ServerAddress;

                /*
                if (string.IsNullOrEmpty(ServerAddress))
                    return string.Empty;

                if (char.IsDigit(ServerAddress[0]))
                    return ServerAddress;

                string[] addressPort = ServerAddress.Split(new[] {":"}, StringSplitOptions.None);

                if (addressPort.Length == 0)
                    return string.Empty;

                if (addressPort.Length == 1)
                    return addressPort[0];

                IPAddress[] addresses = Dns.GetHostAddresses(addressPort[0]);

                if (addresses.Length == 0)
                    return string.Empty;

                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    if (!addresses[0].IsIPv4MappedToIPv6() &&
                        addresses[0].AddressFamily == AddressFamily.InterNetwork)
                    {
                        IPAddress ipv6Address = addresses[0].MapToIPv6();

                        if (ipv6Address.AddressFamily == AddressFamily.InterNetworkV6)
                        {
                            string address = string.Format("[{0}]:{1}", ipv6Address.ToString(),
                                int.Parse(addressPort[1]));

                            return address;
                        }
                    }
                }

                IPEndPoint endPoint = new IPEndPoint(addresses[0], int.Parse(addressPort[1]));

                if (endPoint.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    return string.Format("[{0}]:{1}", endPoint.Address, endPoint.Port);
                }

                return endPoint.ToString();
                */
            }
        }
    }
}