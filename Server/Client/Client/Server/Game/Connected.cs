﻿using System;
using Client.Server.Operations;
using Common.Core;
using ExitGames.Client.Photon;

namespace Client.MetaServer.Game
{
    public class Connected : IGameState
    {
        public static IGameState Create(Game game)
        {
            return new Connected(game);
        }

        private readonly Game _game;

        private Connected(Game game)
        {
            _game = game;
        }

        public GameState State
        {
            get { return GameState.Connected; }
        }

        private int _operationId = 0;

        private int NextOperationId
        {
            get { return ++_operationId; }
        }

        public void OnActive()
        {
            _game.OperationManager.AddOperation(new Cl2Game_TryAttack());

            _operationId = 0;
        }

        public void OnDeactive()
        {
            _game.OperationManager.RemoveOperation(OperationType.Cl2Game_TryAttack);
        }

        public void OnEventReceive(EventData eventData)
        {
            throw new NotImplementedException();
        }

        public void OnOperationResponce(OperationResponse operation)
        {
            throw new NotImplementedException();
        }

        public bool OnStatusChanged(StatusCode statusCode)
        {
            throw new NotImplementedException();
        }

        public void OnUpdate()
        {
            throw new NotImplementedException();
        }
    }
}