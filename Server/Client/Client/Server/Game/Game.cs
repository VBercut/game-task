﻿using System;
using Client.MetaServer.Game.Entities;
using Common.Core;
using ExitGames.Client.Photon;

namespace Client.MetaServer.Game
{
    public class Game : IPhotonPeerListener
    {
        public static Game Instance { get; private set; }

        private IGameState _state;

        public Client Client { get; private set; }

        public IGameListener Listener { get; private set; }
        public ServerSettings Settings { get; private set; }

        public PlayerItem Avatar { get; private set; }

        public World.World World { get; private set; }

        public IOperationManager OperationManager
        {
            get { return Client.OperationManager; }
        }

        public IGameState State
        {
            get { return _state; }

            set
            {
                if (_state != null)
                {
                    _state.OnDeactive();
                }

                _state = value;
                _state.OnActive();
            }
        }

        public int ServerTime
        {
            get { return Client.ServerTimeInMilliSeconds; }
        }

        public int Ping
        {
            get { return Client.RoundTripTime; }
        }

        #region Init

        public Game(IGameListener listener, ServerSettings settings)
        {
            Listener = listener;
            Settings = settings;

            State = Disconnected.Create(this);

            Instance = this;
        }

        public bool Init(Client client)
        {
            Client = client;
            State = WaitingForConnect.Create(this);

            if (!client.Connect(Settings.ResultServerAddress, Settings.ServerName))
            {
                return false;
            }

            return true;
        }

        #endregion

        #region Public API

        public void SetConnected()
        {
            State = Connected.Create(this);
            Listener.OnConnect(this);
        }

        public void SetDisconnected(StatusCode statusCode)
        {
            Client.Disconnect();

            State = Disconnected.Create(this);
            Listener.OnDisconnect(this, statusCode);

            Client.Service();
        }

        public void OnUpdate()
        {
            if (State.State != GameState.Disconnected)
            {
                Client.Service();
                State.OnUpdate();
            }
        }

        public bool Request(OperationType type, IOperationHandler handler, bool reliable = false)
        {
            return Request(type, handler, null, reliable);
        }

        public bool Request(OperationType type, IOperationHandler handler,
            Action<IOperationHandler, OperationResultCode> callback, bool reliable = false)
        {
            if (Client == null)
            {
                return false;
            }

            return Client.OperationManager.Request(type, handler, callback, reliable);
        }

        public void SetActiveWorld(World.World world)
        {
            World = world;
        }

        #endregion

        #region Photon API

        public void DebugReturn(DebugLevel level, string message)
        {
        }

        public void OnOperationResponse(OperationResponse operationResponse)
        {
            State.OnOperationResponce(operationResponse);
        }

        public void OnStatusChanged(StatusCode statusCode)
        {
            State.OnStatusChanged(statusCode);
        }

        public void OnEvent(EventData eventData)
        {
            State.OnEventReceive(eventData);
        }

        #endregion
    }
}