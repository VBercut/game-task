﻿using Client.MetaServer.Game.World.Objects;
using Common.Core.Events;
using Common.Core.World;

namespace Client.MetaServer.Game
{
    public class BaseWorldRules : WorldObject, IRules
    {
        public virtual void OnGameEnd(Game2Cl_EndGame data)
        {
        }
    }
}