﻿using ExitGames.Client.Photon;

namespace Client.MetaServer.Game
{
    public class Disconnected : IGameState
    {
        public static IGameState Create(Game game)
        {
            return new Disconnected(game);
        }

        protected readonly Game _game;

        protected Disconnected(Game game)
        {
            _game = game;
        }

        public GameState State
        {
            get { return GameState.Disconnected; }
        }

        public void OnActive()
        {
        }

        public void OnDeactive()
        {
        }

        public void OnEventReceive(EventData eventData)
        {
        }

        public void OnOperationResponce(OperationResponse operation)
        {
        }

        public bool OnStatusChanged(StatusCode statusCode)
        {
            return false;
        }

        public void OnUpdate()
        {
        }
    }
}