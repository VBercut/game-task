﻿using ExitGames.Client.Photon;

namespace Client.MetaServer.Game
{
    public class WaitingForConnect : IGameState
    {
        public static IGameState Create(Game game)
        {
            return new WaitingForConnect(game);
        }

        protected readonly Game _game;

        protected WaitingForConnect(Game game)
        {
            _game = game;
        }

        public GameState State
        {
            get { return GameState.WaitForConnect; }
        }


        public void OnActive()
        {
        }

        public void OnDeactive()
        {
        }

        public void OnUpdate()
        {
        }

        public void OnEventReceive(EventData eventData)
        {
        }

        public void OnOperationResponce(OperationResponse operation)
        {
        }

        public bool OnStatusChanged(StatusCode statusCode)
        {
            switch (statusCode)
            {
                case StatusCode.Connect:
                {
                    _game.SetConnected();
                    break;
                }

                case StatusCode.Disconnect:
                case StatusCode.DisconnectByServer:
                case StatusCode.DisconnectByServerLogic:
                case StatusCode.DisconnectByServerUserLimit:
                case StatusCode.TimeoutDisconnect:
                {
                    _game.SetDisconnected(statusCode);
                    break;
                }

                default:
                    return false;
            }

            return true;
        }
    }
}