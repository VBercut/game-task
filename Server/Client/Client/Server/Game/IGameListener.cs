﻿using Client.MetaServer.Game.Entities;
using Client.MetaServer.Game.World.Objects;
using ExitGames.Client.Photon;
using UnityEngine;

namespace Client.MetaServer.Game
{
    public interface IGameListener
    {
        void OnConnect(Game game);

        void OnDisconnect(Game game, StatusCode statusCode);

        void OnSpawnLocalPlayer(Game game, Item item, string avatar, Vector3 position);

        void OnWorldObjectAdded(WorldObject worldObject);

        void OnItemAdded(Game game, Item item);

        void OnItemRemoved(Game game, Item item);

        void OnKill(Game game, string killer, string target);

        void OnGameEnd(bool win);
    }
}