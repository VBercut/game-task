﻿using System.Collections.Generic;
using System.Reflection;
using Common.Core.World;
using Common.Core.World.Serialization;
using Common.Core.World.WorldObjects;
using Common.Utils;
using UnityEngine;
using Vector3 = Common.Data.Vector3;

namespace Client.MetaServer.Game.World.Objects
{
    public class WorldObjectId : MonoBehaviour
    {
        private static int _currentId;

        public int id;

        public void BuildId()
        {
            _currentId++;
            id = _currentId;
        }

        public static void ResetOffset()
        {
            _currentId = 0;
        }
    }

    public abstract class WorldObject : MonoBehaviour, IWorldObject
    {
        protected Dictionary<byte, FieldInfo> _syncFields;

        protected virtual Dictionary<byte, FieldInfo> SyncFields
        {
            get
            {
                if (_syncFields == null)
                {
                    _syncFields = new Dictionary<byte, FieldInfo>();

                    var syncPropertyType = typeof(SyncPropertyAttribute);

                    var fields = GetType().GetFields();

                    for (int i = 0; i < fields.Length; i++)
                    {
                        var field = fields[i];

                        if (field.MemberType != MemberTypes.Field)
                        {
                            continue;
                        }

                        object[] attributes = field.GetCustomAttributes(syncPropertyType, true);

                        if (attributes.Length == 0)
                        {
                            continue;
                        }

                        SyncPropertyAttribute attribute = null;

                        for (int attributeIndex = 0; attributeIndex < attributes.Length; attributeIndex++)
                        {
                            if (attributes[attributeIndex].GetType() == syncPropertyType)
                            {
                                attribute = (SyncPropertyAttribute) attributes[attributeIndex];
                                break;
                            }
                        }

                        if (attribute == null)
                        {
                            continue;
                        }

                        _syncFields.Add((byte) attribute.Type, field);
                    }
                }

                return _syncFields;
            }
        }

        private int _id = -1;

        public int Id
        {
            get
            {
                if (_id < 0)
                {
                    _id = GetComponent<WorldObjectId>().id;
                }

                return _id;
            }
        }

        public string Type
        {
            get { return GetType().Name; }
        }

        public Vector3 Position
        {
            get { return new Vector3(transform.position.x, transform.position.y, transform.position.z); }
        }

        public Vector3 Rotation
        {
            get
            {
                return new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y,
                    transform.rotation.eulerAngles.z);
            }
        }

        public virtual IWorldObject Parent
        {
            get
            {
                if (transform.parent == null)
                {
                    return null;
                }

                WorldObject parent = transform.parent.GetComponent<WorldObject>();

                return parent;
            }
        }

        public virtual bool Updatable
        {
            get { return !gameObject.isStatic; }
        }

        public bool Enabled
        {
            get { return gameObject.active; }
            set
            {
                if (gameObject == null)
                {
                    return;
                }

                if (gameObject.active != value)
                {
                    gameObject.active = value;
                    OnEnabledChanged();
                }
            }
        }

        public bool Replication { get; set; }

        public void Start()
        {
            Replication = false;
            OnStart();

            Game.Instance.Listener.OnWorldObjectAdded(this);
        }

        public void OnEnable()
        {
            OnObjectEnable();
        }

        public void OnDisable()
        {
            OnObjectDisable();
        }

        public virtual void Init(Dictionary<byte, object> properties)
        {
        }

        public virtual void HandleEvent(int eventId, Dictionary<byte, object> data)
        {
        }

        public virtual void OnUpdate()
        {
        }

        protected virtual void OnPropertyChanged(byte name)
        {
        }

        protected virtual void OnEnabledChanged()
        {
        }

        public bool GetProperty(byte name, out object value)
        {
            value = null;

            FieldInfo fieldInfo;

            if (!SyncFields.TryGetValue(name, out fieldInfo))
            {
                return false;
            }

            value = fieldInfo.GetValue(this);
            return true;
        }

        public bool SetProperty(byte name, object value)
        {
            if (name == (byte) PropertyTypes.Enabled)
            {
                Enabled = (bool) value;
                return true;
            }

            FieldInfo fieldInfo;

            if (!SyncFields.TryGetValue(name, out fieldInfo))
            {
                return false;
            }

            fieldInfo.SetValue(this, value);
            OnPropertyChanged(name);
            return true;
        }

        public virtual void OnStart()
        {
        }

        public virtual void OnObjectEnable()
        {
        }

        public virtual void OnObjectDisable()
        {
        }

        public void RebuildId()
        {
            if (gameObject.GetComponent<WorldObjectId>() != null)
            {
                foreach (var worldObjectId in gameObject.GetComponents<WorldObjectId>())
                {
                    DestroyImmediate(worldObjectId);
                }
            }

            WorldObjectId idComponent = gameObject.AddComponent<WorldObjectId>();
            idComponent.BuildId();

            _id = -1;
            _id = Id;
        }

        public WorldObjectSpecWrapper ToSpecWrapper()
        {
            WorldObjectSpecWrapper wrapper = new WorldObjectSpecWrapper
            {
                id = Id,
                enabled = Enabled,
                parent = Parent == null ? -1 : Parent.Id,
                position = Position,
                rotation = Rotation,
                type = Type,
                updatable = Updatable,
                neighbours = new List<int>()
            };

            WorldObject[] neighbours = GetComponents<WorldObject>();
            for (int i = 0; i < neighbours.Length; i++)
            {
                WorldObject neighbour = neighbours[i];

                if (neighbour != this)
                {
                    wrapper.neighbours.Add(neighbour.Id);
                }
            }

            List<WorldObjectPropertyWrapper> properties = new List<WorldObjectPropertyWrapper>();
            var fields = this.GetType().GetFields();

            for (int i = 0; i < fields.Length; i++)
            {
                var field = fields[i];

                SyncPropertyAttribute attribute;

                if (!field.GetAttribute(out attribute))
                {
                    continue;
                }

                WorldObjectPropertyWrapper property = new WorldObjectPropertyWrapper
                {
                    name = (byte) attribute.Type,
                    value = field.GetValue(this) == null ? new WorldObjectId {id = -1} : field.GetValue(this)
                };

                WorldObject anotherObject = property.value as WorldObject;

                if (anotherObject != null)
                {
                    property.value = new WorldObjectId {id = anotherObject.Id};
                }

                properties.Add(property);
            }

            wrapper.properties = properties;

            return wrapper;
        }
    }
}