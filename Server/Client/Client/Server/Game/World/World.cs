﻿using System.Collections.Generic;
using Client.MetaServer.Game.World.Objects;
using Common.Core.World;
using Common.Core.World.Serialization;
using LitJson;
using UnityEngine;

namespace Client.MetaServer.Game.World
{
    public class World : MonoBehaviour, IWorld
    {
        public string WorldName;
        public BaseWorldRules WorldRules;

        private float _startTime = 0.0f;

        private readonly Dictionary<int, WorldObject> _worldObjects = new Dictionary<int, WorldObject>();

        public string Name
        {
            get { return WorldName; }
        }

        public IWorldObject[] Objects
        {
            get { return null; }
        }

        public IRules Rules
        {
            get { return WorldRules; }
        }

        private void ReqisterChildren(Transform obj)
        {
            for (int i = 0; i < obj.childCount; i++)
            {
                Transform child = obj.GetChild(i);

                WorldObject worldObject = child.GetComponent<WorldObject>();

                if (worldObject != null)
                {
                    AddWorldObject(worldObject);
                }

                ReqisterChildren(child);
            }
        }

        private void AddWorldObject(WorldObject worldObject)
        {
            if (worldObject.Id == -1)
            {
                return;
            }

            _worldObjects[worldObject.Id] = worldObject;
        }

        private void FillObjects(Transform root, List<IWorldObject> objects)
        {
            WorldObject[] sceneObjects = root.GetComponents<WorldObject>();

            if (sceneObjects != null)
            {
                for (int i = 0; i < sceneObjects.Length; i++)
                {
                    objects.Add(sceneObjects[i]);
                }
            }

            for (int i = 0; i < root.childCount; i++)
            {
                FillObjects(root.GetChild(i), objects);
            }
        }

        void Start()
        {
            _startTime = Time.time;

            ReqisterChildren(transform);

            Game.Instance.SetActiveWorld(this);
        }

        public bool Load(string name)
        {
            return true;
        }

        public void Update()
        {
        }

        public string Serialize()
        {
            if (Rules == null)
            {
                return string.Empty;
            }

            List<IWorldObject> objects = new List<IWorldObject>();
            FillObjects(transform, objects);

            for (int objIndex = 0; objIndex < objects.Count; objIndex++)
            {
                WorldObject worldObject = objects[objIndex] as WorldObject;
                worldObject.RebuildId();
            }

            WorldSpecWrapper wrapper = new WorldSpecWrapper
            {
                name = Name,
                rules = (Rules as BaseWorldRules).Id,
                objects = new List<WorldObjectSpecWrapper>()
            };

            for (int objIndex = 0; objIndex < objects.Count; objIndex++)
            {
                WorldObject worldObject = objects[objIndex] as WorldObject;
                wrapper.objects.Add(worldObject.ToSpecWrapper());
            }

            return JsonMapper.ToJson(wrapper);
        }
    }
}