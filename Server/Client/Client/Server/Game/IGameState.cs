﻿using ExitGames.Client.Photon;

namespace Client.MetaServer.Game
{
    public enum GameState
    {
        Connected,
        Disconnected,
        WaitForConnect,
        InGame
    }

    public interface IGameState
    {
        GameState State { get; }

        void OnActive();

        void OnDeactive();

        void OnEventReceive(EventData eventData);

        void OnOperationResponce(OperationResponse operation);

        bool OnStatusChanged(StatusCode statusCode);

        void OnUpdate();
    }
}