﻿using Client.MetaServer.Operations;
using Common.Core;
using Common.Data;
using ExitGames.Client.Photon;

namespace Client.MetaServer
{
    public class Client : PhotonPeer
    {
        public OperationManager OperationManager { get; private set; }
        public ServerType ServerType { get; private set; }

        public Client(IPhotonPeerListener listener, ConnectionProtocol protocolType, ServerType serverType)
            : base(listener, protocolType)
        {
            ChannelCount = ServerSettings.ChannelsCount;
            ServerType = serverType;

            OperationManager = new OperationManager(this);
        }
    }
}