﻿namespace Common.Data
{
    public enum DataOperationResult
    {
        Okay,
        Error
    }

    public abstract class DataOperation
    {
        public virtual DataOperationResult Result { get; protected set; }

        public abstract IDataProxy CreateDataProxy { get; }

        public void Process(IDataBase dataBase)
        {
            using (IDataCommand command = dataBase.CreateCommand())
            {
                command.InData.Clear();
                command.OutData.Clear();

                OnProcess(command);
                Result = command.Execute();

                if (Result != DataOperationResult.Error)
                {
                    OnReadData(command);
                }
            }
        }

        protected abstract void OnProcess(IDataCommand command);

        protected virtual void OnReadData(IDataCommand command)
        {
        }
    }
}