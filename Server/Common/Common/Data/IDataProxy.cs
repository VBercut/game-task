﻿using System.Collections.Generic;

namespace Common.Data
{
    public interface IDataProxy
    {
        string RawData { get; }

        IList<string> FieldsNames { get; }

        bool ReadField<T>(string name, out T value);
        void WriteField<T>(string name, T value);
    }
}