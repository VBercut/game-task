﻿using System;

namespace Common.Data
{
    public interface IDataTransaction : IDisposable
    {
        void Commmit();
        void Rollback();
    }
}