﻿using System;
using System.Collections.Generic;

namespace Common.Data
{
    public interface IDataCommand : IDisposable, ICloneable
    {
        IDataTransaction Transaction { get; set; }

        IList<IDataProxy> InData { get; }
        IList<IDataProxy> OutData { get; }

        DataOperationResult Execute();

        void Cancel();
    }
}