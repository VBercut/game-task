﻿namespace Common.Data
{
    public enum ServerType
    {
        Meta,
        Game
    }

    public class Server
    {
        public Server(ServerType serverType, string ip, int port, string region, int maxConnections)
        {
            ServerType = serverType;
            Ip = ip;
            Port = port;
            Region = region;
            MaxConnections = maxConnections;
        }

        public ServerType ServerType { get; private set; }
        public string Ip { get; private set; }
        public int Port { get; private set; }
        public string Region { get; private set; }
        public int MaxConnections { get; private set; }
    }
}