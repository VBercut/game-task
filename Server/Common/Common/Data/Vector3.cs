﻿using System;

namespace Common.Data
{
    // TODO: Pack entries to HashCode
    [Serializable]
    public struct Vector3
    {
        public static readonly Vector3 Zero = new Vector3(0, 0, 0);
        public static readonly Vector3 One = new Vector3(1, 1, 1);
        public static readonly Vector3 Forward = new Vector3(0, 0, 1);

        public float x;
        public float y;
        public float z;

        public Vector3 Normalized
        {
            get
            {
                float magnitude = Magnitude;

                return new Vector3(x / magnitude, y / magnitude, z / magnitude);
            }
        }

        public float Magnitude => (float) Math.Sqrt(x * x + y * y + z * z);

        public float Normalize
        {
            get
            {
                float magnitude = Magnitude;
                x = x / magnitude;
                y = y / magnitude;
                z = z / magnitude;

                return magnitude;
            }
        }

        public Vector3(Vector3 another)
        {
            x = another.x;
            y = another.y;
            z = another.z;
        }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }

        public static Vector3 operator *(Vector3 v1, float scalar)
        {
            return new Vector3(v1.x * scalar, v1.y * scalar, v1.z * scalar);
        }

        public static Vector3 Lerp(Vector3 v1, Vector3 v2, float t)
        {
            return new Vector3(v1.x + (v2.x - v1.x) * t,
                v1.y + (v2.y - v1.y) * t,
                v1.z + (v2.z - v1.z) * t);
        }

        public static float Distance(Vector3 v1, Vector3 v2)
        {
            return (v1 - v2).Magnitude;
        }

        public static float Dot(Vector3 v1, Vector3 v2)
        {
            return (float) (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
        }

        public override bool Equals(object obj)
        {
            Vector3 anotherVector = (Vector3) obj;

            return Math.Abs(x - anotherVector.x) < float.Epsilon &&
                   Math.Abs(y - anotherVector.y) < float.Epsilon &&
                   Math.Abs(z - anotherVector.z) < float.Epsilon;
        }
    }
}