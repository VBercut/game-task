﻿namespace Common.Data
{
    public enum ReturnCode
    {
        Ok = 0,
        Fatal = 1,

        ParameterIsOutOfRange = 25,
        OperationIsNotSupported,
        InvalidOperationParametr,
        InvalidOperation
    }
}