﻿using System;

namespace Common.Data
{
    public interface IDataBase : IDisposable
    {
        bool Init();

        IDataCommand CreateCommand();
        IDataTransaction BeginTransaction();
    }
}