﻿namespace Common.Core
{
    public interface IServerPeer
    {
        string ConnectionString { get; }
    }
}