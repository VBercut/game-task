﻿using System.Collections.Generic;

namespace Common.Core
{
    public interface IOperationData
    {
        Dictionary<byte, object> Serialize();
        bool Deserialize(Dictionary<byte, object> data);
    }
}