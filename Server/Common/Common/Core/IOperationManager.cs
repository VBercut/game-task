﻿using System.Collections.Generic;
using Common.Data;

namespace Common.Core
{
    public interface IOperationManager
    {
        void AddOperation(IOperation operation);
        void RemoveOperation(OperationType operation);
        void Update();

        Dictionary<byte, object> ProcessRequest(OperationType type, Dictionary<byte, object> data,
            out ReturnCode result);
        bool ProcessResponce(OperationType type, Dictionary<byte, object> data, ReturnCode result);
    }
}