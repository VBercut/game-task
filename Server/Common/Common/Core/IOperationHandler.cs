﻿using System;
using System.Collections.Generic;
using Common.Data;

namespace Common.Core
{
    public enum OperationResultCode
    {
        Success,
        InProgress,
        Failed
    }

    public interface IOperationHandler : IDisposable
    {
        IOperationData Request { get; }
        IOperationData Responce { get; }

        OperationResultCode Start(Dictionary<byte, object> data);
        OperationResultCode Update();
        OperationResultCode Finish();

        void WaitResponce();
        void ProcessFail(ReturnCode code);
    }
}