﻿using System.Collections.Generic;
using Common.Data;

namespace Common.Core.World
{
    public interface IWorldObject
    {
        int Id { get; }
        string Type { get; }

        Vector3 Position { get; }
        Vector3 Rotation { get; }

        IWorldObject Parent { get; }

        bool Updatable { get; }
        bool Enabled { get; set; }

        void Init(Dictionary<byte, object> properties);

        void OnUpdate();

        bool GetProperty(byte name, out object value);
        bool SetProperty(byte name, object value);
    }
}