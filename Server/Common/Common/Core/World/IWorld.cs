﻿namespace Common.Core.World
{
    public interface IWorld
    {
        string Name { get; }

        IWorldObject[] Objects { get; }
        IRules Rules { get; }

        bool Load(string name);

        void Update();
    }
}