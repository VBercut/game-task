﻿
using Common.Core.Events;

namespace Common.Core.World
{
    public interface IRules
    {
        void OnGameEnd(Game2Cl_EndGame data);
    }
}