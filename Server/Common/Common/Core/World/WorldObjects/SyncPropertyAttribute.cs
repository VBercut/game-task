﻿using System;

namespace Common.Core.World.WorldObjects
{
    [AttributeUsage(AttributeTargets.Field)]
    public class SyncPropertyAttribute : Attribute
    {
        public readonly PropertyTypes Type;

        public SyncPropertyAttribute(PropertyTypes type)
        {
            Type = type;
        }
    }
}