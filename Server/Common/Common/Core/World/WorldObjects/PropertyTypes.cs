﻿namespace Common.Core.World.WorldObjects
{
    public enum PropertyTypes
    {
       Hp,
       HpMax,
       Name,
       Velocity,
       Enabled 
    }
}