﻿using System;
using System.Collections.Generic;
using Common.Data;

namespace Common.Core.World.Serialization
{
    [Serializable]
    public class WorldObjectId
    {
        public int id;
    }

    [Serializable]
    public class WorldObjectPropertyWrapper
    {
        public byte name;
        public object value;
    }

    [Serializable]
    public class WorldObjectSpecWrapper
    {
        public int id;

        public string type;

        public Vector3 position;
        public Vector3 rotation;

        public int parent;

        public bool updatable;
        public bool enabled;

        public List<WorldObjectPropertyWrapper> properties;
        public List<int> neighbours;
    }

    [Serializable]
    public class WorldSpecWrapper
    {
        public string name;
        public int rules;
        public List<WorldObjectSpecWrapper> objects;
    }
}