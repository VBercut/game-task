﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Core.Events
{
    public enum EventDataType : byte
    {
        Meta2Cl_UpdateScore,
        Meta2Cl_EndGame
    }

    public interface IEventHandler
    {
        EventData Data { get; }

        bool Process();
    }

    public class EventHandler<T> : IEventHandler where T : EventData
    {
        public T data;
        public Action<T> handler;

        public EventData Data => data;

        public EventHandler(Action<T> handler)
        {
            this.handler = handler;
            data = Activator.CreateInstance<T>();
        }

        public bool Process()
        {
            handler(data);
            return true;
        }
    }

    public interface IEventCreator
    {
        IEventHandler CreateEvent(EventDataType type);
    }

    public abstract class EventData : IOperationData
    {
        private static readonly Type _hashTable = typeof(Hashtable);

        public abstract EventDataType Type { get; }

        public static IEventHandler Read(IEventCreator eventCreator, Dictionary<byte, object> data)
        {
            if (!data.ContainsKey((byte) OperationFields.OperationId))
            {
                return null;
            }

            EventDataType type = (EventDataType) data[(byte) OperationFields.OperationId];

            IEventHandler handler = eventCreator.CreateEvent(type);

            EventData eventData = handler.Data;
            eventData.Deserialize(data);

            return handler;
        }

        public Dictionary<byte, object> Serialize()
        {
            Dictionary<byte, object> data = new Dictionary<byte, object>();

            Type type = this.GetType();
            var fields = type.GetFields();

            for (byte i = 0; i < fields.Length; i++)
            {
                data[i] = fields[i].GetValue(this);
            }
            data[(byte) OperationFields.OperationId] = Type;

            return data;
        }

        public bool Deserialize(Dictionary<byte, object> data)
        {
            Type type = this.GetType();
            var fields = type.GetFields();

            for (byte i = 0; i < fields.Length; i++)
            {
                fields[i].SetValue(this, data[i]);
            }

            return true;
        }
    }
}