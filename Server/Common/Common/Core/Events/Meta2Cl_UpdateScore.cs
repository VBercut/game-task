﻿namespace Common.Core.Events
{
    public class Meta2Cl_UpdateScore : EventData
    {
        public override EventDataType Type => EventDataType.Meta2Cl_UpdateScore;

        public int[] id;

        public int[] wins;
        public int[] loses;
    }
}