﻿namespace Common.Core.Events
{
    public class Game2Cl_EndGame : EventData
    {
        public override EventDataType Type => EventDataType.Meta2Cl_EndGame;

        public bool win;
    }
}