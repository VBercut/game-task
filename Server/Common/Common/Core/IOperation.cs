﻿using System;

namespace Common.Core
{
    public enum OperationFields : byte
    {
        OperationId = 254
    }

    public interface IOperation : IDisposable
    {
        OperationType Type { get; }
        IOperationManager Manager { get; set; }
        IOperationHandler CreateHandler();
    }
}