﻿using System.Collections.Generic;
using Common.Data;

namespace Common.Core.Operations
{
    public abstract class BaseOperationHandler<TRequest, TResponce> : IOperationHandler
        where TRequest : IOperationData where TResponce : IOperationData
    {
        public IOperationData Request { get; protected set; }
        public IOperationData Responce { get; protected set; }

        public TRequest RequestData => (TRequest) Request;
        public TResponce ResponceData => (TResponce) Responce;

        public OperationResultCode Start(Dictionary<byte, object> data)
        {
            ReadData(data);

            return OnStart();
        }

        public virtual OperationResultCode OnStart()
        {
            return OperationResultCode.Success;
        }

        public OperationResultCode Update()
        {
            return OnUpdate();
        }

        public virtual OperationResultCode OnUpdate()
        {
            return OperationResultCode.Success;
        }

        public OperationResultCode Finish()
        {
            return OnFinish();
        }

        public virtual OperationResultCode OnFinish()
        {
            return OperationResultCode.Success;
        }

        protected abstract void ReadData(Dictionary<byte, object> data);

        public virtual void WaitResponce()
        {
        }

        public virtual void ProcessFail(ReturnCode code)
        {
        }

        public virtual void Dispose()
        {
        }
    }
}