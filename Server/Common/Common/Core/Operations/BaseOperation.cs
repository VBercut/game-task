﻿using System;

namespace Common.Core.Operations
{
    public abstract class BaseOperation : IOperation
    {
        public abstract OperationType Type { get; }
        public IOperationManager Manager { get; set; }

        public virtual IOperationHandler CreateHandler()
        {
            throw new NotImplementedException();
        }

        public virtual void Dispose()
        {
        }
    }
}