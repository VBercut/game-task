﻿namespace Common.Core.Operations
{
    public enum LoginResult
    {
        Fail = -100,
        ProtocolIsInvalid = -200,
        Success = 0
    }

    public abstract class Cl2Meta_Login : BaseOperation
    {
        public const string LOGIN_VERSION = "0.1";

        public override OperationType Type => OperationType.Cl2Meta_Login;

        public class Request : OperationData
        {
            public string guid;
            public string loginVersion = LOGIN_VERSION;
        }

        public class Responce : OperationData
        {
            public LoginResult result;

            public int id;
            public string token;
            public string name;
        }
    }
}