﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Common.Core
{
    public class OperationData : IOperationData
    {
        public Dictionary<byte, object> Serialize()
        {
            Dictionary<byte, object> data = new Dictionary<byte, object>();
            Type type = this.GetType();

            FieldInfo[] fields = type.GetFields();

            for (int i = 0; i < fields.Length; i++)
            {
                data[(byte) i] = fields[i].GetValue(this);
            }

            return data;
        }

        public bool Deserialize(Dictionary<byte, object> data)
        {
            Type type = this.GetType();

            FieldInfo[] fields = type.GetFields();

            for (int i = 0; i < fields.Length; i++)
            {
                fields[i].SetValue(this, data[(byte) i]);
            }

            return true;
        }
    }
}