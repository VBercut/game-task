﻿using System;
using System.Reflection;

namespace Common.Utils
{
    public static class AssemblyExtensions
    {
        public static bool GetAttribute<T>(this FieldInfo field, out T attribute) where T : Attribute
        {
            attribute = null;

            if (field.MemberType != MemberTypes.Field)
            {
                return false;
            }

            Type attributeType = typeof(T);

            object[] attributes = field.GetCustomAttributes(attributeType, true);

            for (int i = 0; i < attributes.Length; i++)
            {
                var fieldAttribute = attributes[i];

                if (fieldAttribute.GetType() == attributeType)
                {
                    attribute = (T) fieldAttribute;
                    return true;
                }
            }

            return false;
        }
    }
}