﻿using Common.Data;

namespace Common
{
    public class StaticData
    {
        public static StaticData Instance { get; } = new StaticData();

        public Server MetaServer { get; private set; }
        public Server GameServer { get; private set; }

        private StaticData()
        {
        }

        static StaticData()
        {
            Instance.MetaServer = new Server(ServerType.Meta, "localhost", 5040, "ru", 100000);
            Instance.GameServer = new Server(ServerType.Game, "localhost", 4531, "ru", 10);
        }
    }
}