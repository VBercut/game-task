﻿using Client.MetaServer;
using Client.MetaServer.Operations;
using Common.Core;
using Game;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    public Text StateText;
    public Transform LoadingContainer;

    void Awake()
    {
        LoadingContainer.gameObject.SetActive(true);
    }

    void Start()
    {
        MetaServerClient.Instance.OnChangeConnectState += OnChangeConnectState;

        OnChangeConnectState(MetaServerClient.Instance.State);

        Invoke("AutoConnet", 1.0f);
    }

    void AutoConnet()
    {
        MetaServerClient.Instance.OnConnect();
    }

    private void OnChangeConnectState(MetaServerConnectStates state)
    {
        StateText.text = state.ToString();

        if (state == MetaServerConnectStates.Connect)
        {
            LoadingContainer.gameObject.SetActive(false);
            Login();
        }
    }

    void Login()
    {
        Cl2Meta_Login.SendRequest(MetaServerClient.Instance.MetaServer, MetaServerClient.Instance.Guid, OnLogin);
    }

    private void OnLogin(IOperationHandler arg1, OperationResultCode arg2)
    {
        Debug.Log("LOGIN!");
    }

    void OnDestroy()
    {
        MetaServerClient.Instance.OnChangeConnectState -= OnChangeConnectState;
    }
}