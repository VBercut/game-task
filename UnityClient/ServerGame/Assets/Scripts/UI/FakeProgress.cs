﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeProgress : MonoBehaviour {

    public float velocity = 0.5f;
    public Image graphics;

    private float _progress;
	
	void Update ()
	{
	    graphics.fillAmount = Mathf.Repeat(_progress, 1.0f);

	    _progress += Time.smoothDeltaTime * velocity;
	}
}
