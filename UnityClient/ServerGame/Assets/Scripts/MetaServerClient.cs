﻿using System;
using Client.MetaServer;
using Common.Data;
using ExitGames.Client.Photon;
using UnityEngine;

namespace Game
{
    public class MetaServerClient : IMetaServerListener
    {
        private static MetaServerClient _instance;

        public static MetaServerClient Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MetaServerClient();
                    _instance.Init();
                }

                return _instance;
            }
        }

        public event Action<MetaServerConnectStates> OnChangeConnectState;

        private readonly GameSettings _settings = new GameSettings();
        private MetaServer _metaServer;
        private global::Client.MetaServer.Client _client;

        public MetaServerConnectStates State
        {
            get { return _metaServer.State; }
        }

        public MetaServer MetaServer
        {
            get { return _metaServer; }
        }

        public GameSettings GameSettings
        {
            get { return _settings; }
        }

        // TODO: JUST FOR TEST (NOT SAFE)
        public string Guid
        {
            get
            {
                string guid = PlayerPrefs.GetString("ACCOUNT.GUID", string.Empty);

                if (string.IsNullOrEmpty(guid))
                {
                    guid = Guid = System.Guid.NewGuid().ToString();
                }

                return guid;
            }

            set
            {
                PlayerPrefs.SetString("ACCOUNT.GUID", value);
                PlayerPrefs.Save();
            }
        }

        public void Init()
        {
            _metaServer = new MetaServer(this, _settings);
            _client = new global::Client.MetaServer.Client(_metaServer, ConnectionProtocol.Udp, ServerType.Meta);
        }

        public void OnConnect()
        {
            _metaServer.Init(_client);

            if (OnChangeConnectState != null)
            {
                OnChangeConnectState(State);
            }
        }

        public void Disconnect()
        {
            _client.Disconnect();
        }

        public void OnUpdate()
        {
            _metaServer.Update();
        }

        public void Connect()
        {
            if (OnChangeConnectState != null)
            {
                OnChangeConnectState(State);
            }
        }

        public void Login(int accountId, string token)
        {
        }

        public void OnDisconnect(StatusCode statusCode)
        {
            if (OnChangeConnectState != null)
            {
                OnChangeConnectState(State);
            }
        }
    }
}