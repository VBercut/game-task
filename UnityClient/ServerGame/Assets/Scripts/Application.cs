﻿using Game;
using UnityEngine;

public class Application : MonoBehaviour {

    public static Application Instance { get; private set; }

    public float metaServerPeriodUpdate = 0.45f;
    public float gameServerPeriodUpdate = 0.05f;

	void Awake ()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        InvokeRepeating("MetaServerUpdate", metaServerPeriodUpdate, metaServerPeriodUpdate);
        InvokeRepeating("GameServerUpdate", metaServerPeriodUpdate, metaServerPeriodUpdate);
    }

    void MetaServerUpdate()
    {
        MetaServerClient.Instance.OnUpdate();
    }

    void GameServerUpdate()
    {
        
    }

    void OnApplicationQuit()
    {
        MetaServerClient.Instance.Disconnect();
    }
}
