﻿using System;
using Client.MetaServer;

namespace Game
{
    public class GameSettings : ServerSettings
    {
        [Serializable]
        public struct SettingsData
        {
            public string host;
            public byte region;
            public byte sound;
        }

        private static readonly SettingsData _defaultSettings = new SettingsData
        {
            host = "localhost:5040",
            region = 0,
            sound = 1
        };

        public GameSettings()
        {
            ServerName = "MetaServer";
            ServerAddress = _defaultSettings.host;
        }
    }
}